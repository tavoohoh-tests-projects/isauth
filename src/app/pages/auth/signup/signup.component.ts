import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../../services/_index';
import { ModalComponent } from '../../../_shared/modal/modal.component';
import { CommonsService } from '../../../_helpers/_index';

/**
 * Host the SIGN UP page.
 * It allows the user sign up in to the server
 * using the @method signup() defined in "services/authentication.service.ts".
 */

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  submitted = false;
  returnUrl: string;

  @ViewChild(ModalComponent) modalComponent;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private commonsService: CommonsService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setSignupForm();
  }

  /**
   * Initialize signupForm with all its fields
   * email and password
   */
  setSignupForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-zA-Z0-9._%+-]+@([a-z0-9.-])+\.[a-z]{2,5}$')
      ]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  /**
   * Sign up form submit
   * Send the form to the server and sign up the user
   * After a success sign up it log in the user
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    }

    // Sent it to the api service
    this.authenticationService.signup(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.login();
        },
        error => {
          this.commonsService.errorHandler(error);
        });
  }

  /**
   * Login form submit
   * Send the sign up form to the server and log in the user
   */
  login() {
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.commonsService.onNavigate('/');
        },
        error => {
          this.commonsService.errorHandler(error);
        });
  }

  /**
   * Convenience getter for easy access to form fields
   */
  get f() {
    return this.signupForm.controls;
  }

  /**
   * Display terms and conditions modal
   */
  openTermsModal() {
    this.modalComponent.open({
      title: this.translate.get('info.terms')['value'],
      text: this.translate.get('info.terms_text')['value'],
      confirm: this.translate.get('action.ok')['value']
    });
  }

  /**
   * Navigation helper
   */
  onNavigate(route: string) {
    return this.commonsService.onNavigate(route);
  }
}

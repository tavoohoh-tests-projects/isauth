import { Component} from '@angular/core';

/**
 * It is a small component that shows the footer of the dashboard.
 * It is an indispensable component for the basic layout of pages such as USERS.
 */

@Component({
  selector: 'app-footer',
  template: `
    <p>{{ 'info.isAuth_rights' | translate }}</p>
  `,
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  constructor() { }

}

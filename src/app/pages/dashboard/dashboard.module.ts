// main dependencies
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// third-party dependencies
import { NgxPaginationModule } from 'ngx-pagination';
import { TranslateModule } from '@ngx-translate/core';
import { Routes, RouterModule } from '@angular/router';

// shared
import { ModalModule } from '../../_shared/modal/modal.module';

// helpers
import { FilterPipe } from '../../_pipes/filter.pipe';

// components
import { UsersComponent } from './users/users.component';
import { NavigationComponent } from './_layout/navigation/navigation.component';
import { FooterComponent } from './_layout/footer/footer.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: UsersComponent,
      },
    ]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    UsersComponent,
    NavigationComponent,
    FooterComponent,
    FilterPipe
  ],
  imports: [
    RouterModule.forChild(routes),
    TranslateModule,
    NgxPaginationModule,
    ModalModule,
    FormsModule,
    CommonModule
  ]
})
export class DashboardModule { }

// main dependencies
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// main app
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// third-party dependencies
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// shared
import { ModalModule, LanguageComponent } from './_shared/_index';

// helpers
import { JwtInterceptor, ErrorInterceptor, CommonsService } from './_helpers/_index';

// services
import { AuthenticationService, UsersService } from './services/_index';

@NgModule({
  declarations: [
    AppComponent,
    LanguageComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoadingBarRouterModule,
    LoadingBarHttpClientModule,
    ModalModule,
    ToastrModule.forRoot({
      progressBar: true,
      positionClass: 'toast-top-center'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthenticationService,
    CommonsService,
    UsersService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
